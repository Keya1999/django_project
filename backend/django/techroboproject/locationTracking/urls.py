from django.urls import path

from locationTracking import views

urlpatterns = [
    path('locationTracking/', views.Coordinates_CR_View.as_view()),
    path('locationTracking/<int:pk>/', views.Coordinates_CR_View.as_view()),
]