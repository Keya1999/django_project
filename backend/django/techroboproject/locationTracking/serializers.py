from rest_framework import serializers
from .models import tblCoordinates

class CoordinatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = tblCoordinates
        fields = '__all__'