"""
This class checks  the IMEI is valid or Invalid.

The validation is done by following steps:
1. It starts with the rightmost_value doubles the value of every second digit(eg. 4 becomes 8)
2. If the doubling of the rightmost_values is two digit number(eg. 9 * 2 = 18 ) and add the digits of the product(1+8)
3. Take the sum of all the digits in imeisum.
4. Checks if the imeisum is divisible by 10(eg. 50%10 ) then it is valid otherwise invalid

"""
class IMEI_validation:
   "class to check the IMEI "

   def __init__(self, imeinumber):
       self.imei = imeinumber


       #Function for finding and returning sum of digits of a number

   def sumDig(self, rightmost_values):
         quotient = 0
         while rightmost_values > 0:

            quotient = quotient + (rightmost_values % 10)
            rightmost_values = (int(rightmost_values) / 10)

         return int(quotient)


   def ImeiIsValid(self):
        imei_number = self.imei
        #print(imei_number)

        rightmost_values = 0
        imeisum = 0

        #Loop for get the 15 digit of imei_number where i is looping variable.
        for i in range(15, 0, -1):
            #print("value if imei_number is : ",imei_number)
            rightmost_values = (imei_number % 10)


            if i % 2 == 0:

                # Doubling every alternate digit
                rightmost_values = 2 * rightmost_values


            #Finding the sum of digits
            imeisum = imeisum + self.sumDig(rightmost_values)
            imei_number= imei_number/10

            #print("Output: iemisum = ", imeisum)

        return (imeisum%10 == 0)



"""
#myimei = IMEI_validation(356307042441013)
#myimei = IMEI_validation(359632107452945)
#myimei = IMEI_validation(502695398174524)
#hex_num ='000F333536333037303432343431303133' #invalid imei
#hex_num ='000F333539363332313037343532393435'  #valid imei
hex_num ='000F353032363935333938313734353234'  #valid imei
hex_split1 = hex_num[:4]
hex_split2 = hex_num[4:]

if int(hex_split1,16) != 15 :
    print ("wrong size", hex_split1)
else:
    print("15 digit imei")

test1 = len(str(hex_split2))
my_imei = hex_split2[1:test1:2]
print(my_imei)

myimei = IMEI_validation(int(my_imei))
val_imei = myimei.ImeiIsValid()
if val_imei == True:
    print("valid IMEI")
else:
    print("Invalid IMEI")

"""